package com.ver.main;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.ver.main.adapters.LocalDateAdapter;
import com.ver.main.providers.LocalDateProvider;

@ApplicationPath("/rest")
public class RestApp extends Application{
	public Set<Class<?>> getClasses(){
		Set<Class<?>> s = new HashSet<Class<?>>();
		s.add(PriceListWebService.class);
		s.add(LocalDateProvider.class);
		s.add(LocalDateAdapter.class);
		return s;
	}
}
