package com.ver.main.providers;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;


@Provider
public class LocalDateProvider implements ParamConverterProvider {
	
	
	@Override
	public <T> ParamConverter<T> getConverter(
			Class<T> rawType, 
			Type genericType, 
			Annotation[] annotations) 
	{
		if(rawType == LocalDate.class){
			return new ParamConverter<T>(){

				@Override
				public T fromString(String value) {
                    try {
						LocalDate localDate = LocalDate.parse(value, DateTimeFormatter.ISO_LOCAL_DATE);
                        return rawType.cast(localDate);
                    } catch (Exception ex) {
                        throw new BadRequestException(ex);
                    }
				}

				@Override
				public String toString(T value) {
                    if (value == null) {
                        return null;
                    }
					LocalDate localDate = (LocalDate) value;
                    return DateTimeFormatter.ISO_LOCAL_DATE.format(localDate);
				}
			};
		}
		return null;
	}

}
