package com.ver.main.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.ver.main.adapters.LocalDateAdapter;

@XmlRootElement(name="book")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductPrice {
	private final String productName;
	private final BigDecimal productPrice;

	@XmlJavaTypeAdapter(LocalDateAdapter.class)
	private final LocalDate validFrom;

	@XmlJavaTypeAdapter(LocalDateAdapter.class)
	private final LocalDate validTo;

	public ProductPrice(String productName, BigDecimal productPrice, LocalDate validFrom, LocalDate validTo) {
		this.productName = productName;
		this.productPrice = productPrice;
		this.validFrom = validFrom;
		this.validTo = validTo;
	}

	public String getProductName() { return productName; }

	public BigDecimal getProductPrice() { return productPrice; }

	public LocalDate getValidFrom() { return validFrom; }

	public LocalDate getValidTo() { return validTo; }
}

