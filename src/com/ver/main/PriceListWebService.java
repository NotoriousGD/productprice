package com.ver.main;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.time.LocalDate;
import java.util.List;

import javax.ws.rs.Produces;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ver.main.db.DaoJdbcProductPrice;
import com.ver.main.model.ProductPrice;

@Path("/service")	
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PriceListWebService{
	
	DaoJdbcProductPrice dao = DaoJdbcProductPrice.getInstance();
	
	@GET
	@Path("getPrice")
	public BigDecimal getPrice(
			@NotNull @QueryParam("productName") String productName, 
			@NotNull @QueryParam("date") LocalDate date){	

		BigDecimal price;
		
		//check for the presence of db 
		if(dao.getProductId(productName).equals(0)){
			throw new WebApplicationException(Response.status(HttpURLConnection.HTTP_NOT_FOUND).entity("").build());
		}
		
		//return null if product not sale in the current date
		price = dao.getPrice(productName, date);
		if(price.equals(null)){
			throw new WebApplicationException(Response.status(HttpURLConnection.HTTP_NO_CONTENT).entity("").build());
		}

		return  price;
	}


	@GET
	@Path("getPrices")
	public List<ProductPrice> getPrices(@NotNull @QueryParam("productName") String productName) {
		
		//check for the presence of db 
		if(dao.getProductId(productName) == 0){
			throw new WebApplicationException(Response.status(HttpURLConnection.HTTP_NOT_FOUND).entity("").build());
		}
		
		return dao.getPrices(productName);
	}


	@POST
	@Path("setPrice")
	public void setPrice(
			@NotNull @QueryParam("productName") String productName, 
			@NotNull @QueryParam("price") @DecimalMin("0") BigDecimal price, 
			@QueryParam("fromDate") LocalDate fromDate, 
			@QueryParam("toDate") LocalDate toDate) {

		if(fromDate.equals(null))fromDate = LocalDate.of(1000, 01, 01);//min for DateTimeFormatter.ISO_LOCAL_DATE
		if(toDate.equals(null))fromDate = LocalDate.of(9999, 12, 31);//max for DateTimeFormatter.ISO_LOCAL_DATE
		
		if(fromDate.isAfter(toDate)){
			throw new WebApplicationException(
					Response.status(HttpURLConnection.HTTP_INTERNAL_ERROR).entity("").build());
		}

		dao.setPrice(productName, price, fromDate, toDate);
	}


	@POST
	@Path("stopSelling")
	public void stopSelling(
			@NotNull @QueryParam("productName") String productName, 
			@QueryParam("fromDate") LocalDate fromDate, 
			@QueryParam("toDate") LocalDate toDate) {
		
		if(fromDate.equals(null))fromDate = LocalDate.of(1000, 01, 30);
		if(toDate.equals(null))fromDate = LocalDate.of(9999, 12, 31);
		
		
		//check for the presence of db 
		if(dao.getProductId(productName).equals(0)){
			throw new WebApplicationException(Response.status(HttpURLConnection.HTTP_NOT_FOUND).entity("").build());
		}
		
		if(fromDate.isAfter(toDate)){
			throw new WebApplicationException(
					Response.status(HttpURLConnection.HTTP_INTERNAL_ERROR).entity("").build());
		}
		dao.stopSelling(productName, fromDate, toDate);
	}	
}
