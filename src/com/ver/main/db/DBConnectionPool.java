package com.ver.main.db;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class DBConnectionPool {

	private ComboPooledDataSource cPool;
	private String host;
	private String driver;
	private String nameDb;
	private String user;
	private String password;
	private String url;

	private static DBConnectionPool instance;

	public static DBConnectionPool  getInstance(String driver,String host, String nameDb, String user, String password){
		if(instance==null){
			instance = new DBConnectionPool (driver,host, nameDb, user, password);	
		}
		return instance;
	}
	
	private DBConnectionPool(String driver,String host, String nameDb, String user, String password) {
		this.driver = driver;
		this.host = host;
		this.nameDb = nameDb;
		this.user = user;
		this.password = password;
		
		try {
			init();
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		};
	}

	private void init() throws PropertyVetoException{ 			
		//embedded
		url = "jdbc:h2:~/"+nameDb+";AUTO_SERVER=TRUE";

		//server
		//url = "jdbc:h2:tcp:"+host+"/~/"+nameDb+";AUTO_SERVER=TRUE";
		
		//Create DB
		try {	
			Class.forName(driver);

			Connection conn 
			= DriverManager.getConnection(url, user, password);

			Statement stat = conn.createStatement(); 
			ResultSet rs = stat.executeQuery("SHOW TABLES");

			
			if(!rs.next()){
				stat.executeUpdate("create table products("
						+"id bigint not null auto_increment primary key,"
						+"name varchar(255))"
						);

				stat.executeUpdate("create table productdate("
						+"ID bigint auto_increment not null primary key,"
						+" product bigint not null, "
						+" Price DECIMAL,"
						+" validFrom DATE,"
						+" validTo DATE"
						+" ,foreign key (product) references products)"
						);
				//test initial db
				stat.executeUpdate("insert into products(name) values('product1')");
				stat.executeUpdate("insert into products(name) values('product2')");
				stat.executeUpdate("insert into products(name) values('product3')");

				stat.executeUpdate("insert into productdate(product,price,validfrom,validto) values(1,100.12,'1000-01-01','9999-12-31')");
				stat.executeUpdate("insert into productdate(product,price,validfrom,validto) values(2,200.33,'2010-10-10','2013-02-10')");
				stat.executeUpdate("insert into productdate(product,price,validfrom,validto) values(2,300.65,'2013-10-10','2017-02-10')");
				stat.executeUpdate("insert into productdate(product,price,validfrom,validto) values(3,400.56,'1000-01-01','2013-02-10')");
				stat.executeUpdate("insert into productdate(product,price,validfrom,validto) values(3,500,'2019-01-10','9999-12-31')");
				
				System.out.println("new DB created");
			}
			else{
				System.out.println("DB already created");
			}
			rs.close();
			conn.close();
			System.out.println("Server is runned");
		} 

		catch (SQLException e) {e.printStackTrace();} 
		catch (ClassNotFoundException e) {e.printStackTrace();} 

		cPool = new ComboPooledDataSource();
		
		cPool.setDriverClass(driver);
		cPool.setJdbcUrl(url);
		cPool.setUser(user);
		cPool.setPassword(password);
        
		cPool.setMinPoolSize(5);
		cPool.setMaxPoolSize(30);
		cPool.setMaxStatements(180);

		System.out.println("URL:"+url);
	}

	public Connection getConnection(){
		try {
			return cPool.getConnection();
		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}    

	public ResultSet query (Connection H2Connection, String query, String[] parameters){

		ResultSet result = null;
		
		try {
			PreparedStatement state = H2Connection.prepareStatement(query);
			int i = 1;
			for(String param:parameters){
				state.setString(i++, param);
			}
			result = state.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public void updateQuery(Connection H2Connection, String query, String[] parameters){
		try {
			
			PreparedStatement state = H2Connection.prepareStatement(query);
			int i = 1;
			for(String param:parameters){
				state.setString(i++, param);	
			}
			state.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
