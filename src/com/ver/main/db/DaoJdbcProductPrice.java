package com.ver.main.db;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.ver.main.model.ProductPrice;

public class DaoJdbcProductPrice implements DaoProductPrice{


	private static DBConnectionPool controller;
	public Connection connect;

	private DaoJdbcProductPrice() {connect = controller.getConnection();}

	public static DaoJdbcProductPrice instance;

	public static DaoJdbcProductPrice getInstance(){
		if(instance==null){
			controller = DBConnectionPool.getInstance("org.h2.Driver","", "products", "admin", "admin");//embedded 		
			instance = new DaoJdbcProductPrice();
		}
		return instance;
	}


	@Override
	public BigDecimal getPrice(String productName, LocalDate date) {
		BigDecimal price = null;

		ResultSet result =  controller.query(
				connect,
				"SELECT PRODUCTDATE.PRICE "
						+ " FROM PRODUCTDATE "
						+ " INNER JOIN PRODUCTS "
						+ " ON PRODUCTDATE.PRODUCT = PRODUCTS.ID "
						+ " WHERE PRODUCTS.NAME=? and PRODUCTDATE.VALIDFROM <=? and PRODUCTDATE.VALIDTO >=?"
						+ " ORDER BY PRODUCTDATE.ID DESC",
						new String[]{productName,date.toString(),date.toString()});
		try {
			if(result.next()){
				price = result.getBigDecimal("PRICE");
			}				
			result.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return price;
	}

	@Override
	public List<ProductPrice> getPrices(String productName){

		List<ProductPrice> products = new ArrayList<ProductPrice>();

		ResultSet result = controller.query(
				connect,
				"SELECT * from PRODUCTDATE "
						+ "INNER JOIN PRODUCTS "
						+ "ON PRODUCTDATE.PRODUCT = PRODUCTS.ID "
						+" WHERE PRODUCTS.NAME=?",
						new String[]{productName}
				);

		try {
			while(result.next()){
				products.add(new ProductPrice(
						result.getString("NAME"),
						result.getBigDecimal("PRICE"),
						LocalDate.parse(result.getString("VALIDFROM")),
						LocalDate.parse(result.getString("VALIDTO")))
						);
			}
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return products;



	}

	@Override
	public void setPrice(String productName, BigDecimal price, LocalDate fromDate, LocalDate toDate){

		stopSelling(productName, fromDate, toDate);

		int id = getProductId(productName);
		if(id!=0){
			controller.updateQuery(
					connect,
					"INSERT INTO PRODUCTDATE(PRODUCT,PRICE,VALIDFROM,VALIDTO)"
							+" values (?,?,?,?)",
							new String[]{String.valueOf(id),
									String.valueOf(price),
									fromDate.toString(),
									toDate.toString()}
					);	
		}
	}

	@Override
	public void stopSelling(String productName, LocalDate fromDate, LocalDate toDate){

		int id = getProductId(productName);
		if(id!=0){
			ResultSet result = controller.query(connect,"SELECT * FROM PRODUCTDATE WHERE VALIDFROM <=? and VALIDTO >=? and PRODUCT = ?", 
					new String[]{fromDate.toString(),toDate.toString(),String.valueOf(id)});

			try {
				if(result.next()){
					controller.updateQuery(connect,
							"UPDATE PRODUCTDATE SET VALIDTO = DATEADD ('DAY',-1,?)"
									+" WHERE ID = ?",
									new String[]{fromDate.toString(),String.valueOf(result.getInt("ID"))}
							);
					controller.updateQuery(connect,
							"INSERT INTO PRODUCTDATE(PRODUCT,PRICE,VALIDFROM,VALIDTO)"
									+" values (?,?,DATEADD ('DAY',1,?),?)",
									new String[]{String.valueOf(id),String.valueOf(result.getInt("PRICE")),toDate.toString(),result.getString("VALIDTO")}
							);
				}


				controller.updateQuery(connect,
						"DELETE FROM PRODUCTDATE"
								+" WHERE VALIDFROM >=? and VALIDTO <=? and PRODUCT = ?",
								new String[]{fromDate.toString(),toDate.toString(),String.valueOf(id)}
						);

				controller.updateQuery(connect,
						"UPDATE PRODUCTDATE SET VALIDFROM = DATEADD ('DAY',1,?)"
								+" WHERE VALIDFROM BETWEEN ? and ?  and PRODUCT = ?",
								new String[]{toDate.toString(),fromDate.toString(),toDate.toString(),String.valueOf(id)}
						);

				controller.updateQuery(connect,
						"UPDATE PRODUCTDATE SET VALIDFROM = DATEADD ('DAY',-1,?)"
								+" WHERE VALIDFROM BETWEEN ? and ?  and PRODUCT = ?",
								new String[]{fromDate.toString(),toDate.toString(),fromDate.toString(),String.valueOf(id)}
						);

				result.close();		
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 

		}
	}

	public Integer getProductId(String productName){

		ResultSet result = controller.query(connect,
				"SELECT ID FROM PRODUCTS WHERE NAME = ?",
				new String[]{productName}
				);

		int id = 0;
		try {
			if(result.next()){
				id = new Integer(result.getInt("ID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("getID ended id-"+id);
		return id;
	}
}
