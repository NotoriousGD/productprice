package com.ver.main.db;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import com.ver.main.model.ProductPrice;

public interface DaoProductPrice {

	BigDecimal getPrice(String productName,LocalDate date);

	List<ProductPrice> getPrices(String productName); 

	void setPrice(String productName, BigDecimal price, LocalDate fromDate, LocalDate toDate);

	void stopSelling(String productName, LocalDate fromDate, LocalDate toDate);

}
